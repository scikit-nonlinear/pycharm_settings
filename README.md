# Update the PyCharm Run Configurations
From the project directory:

## Win32

``` bat
rmdir /s ..\pycharm_run_configurations\win32
xcopy .idea\runConfigurations ..\pycharm_run_configurations\win32 /O /X /E /H /K
```

## macOS

```bash
rm -rf ../pycharm_run_configurations/macOS
cp -rf .idea/runConfigurations ../pycharm_run_configurations/macOS
```

## Linux

```bash
rm -rf ../pycharm_run_configurations/linux
cp -rf .idea/runConfigurations ../pycharm_run_configurations/linux
```
